#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 17 10:44:46 2020
 
@author: rsolanki
"""
 
import pandas as pd
from sqlalchemy import create_engine
import numpy as np
import re
import psycopg2 as sql
import os 
import datetime as dt
from scipy.optimize import curve_fit
import json

"""
This is a dashboard on cvd-19 which will show the following: 
1. Allow users to select by region: World Wide or Country Wise and show updated date for following: 
        1. Table show # Confirmed, Active, Recovered, and Deaths, and updated data time 
        2. Show a map of the # of Active cases by region selected ( World or country)
        3. Show the # of confirmed cases trend YTD, MA 14, MA30, MA90
        4. Show the # of Active Cases trend YTD, MA 14, MA30, MA90
        5. Show the # of Death Cases trend YTD, MA 14, MA30, MA90
        6. Prediction of # of Confirmed, Active,Death cases by EOY using ARIMA etc
 
        # Updates Once a day
        # https://github.com/CSSEGISandData/COVID-19/blob/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv
"""
 
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = os.path.join(BASE_DIR,"daily_data")

 
def download_files():
    CONFIRMED_WORLD_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv"
    DEA_WORLD_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv"
    REC_WORLD_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv"
    CONFIRMED_USA_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv"
    DEATHS_USA_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_US.csv"
 
    confirmed_world  =  pd.read_csv(CONFIRMED_WORLD_URL,sep=",")
    dea_world        =  pd.read_csv(DEA_WORLD_URL,sep=",")
    recovered_world  =  pd.read_csv(REC_WORLD_URL,sep=",")
    confirmed_usa    =  pd.read_csv(CONFIRMED_USA_URL,sep=",")
    dea_usa          =  pd.read_csv(DEATHS_USA_URL,sep=",")
 
    confirmed_world.to_csv(os.path.join(DATA_DIR,"Confirmed World Cases.csv"))
    dea_world.to_csv(os.path.join(DATA_DIR,"Death World Cases.csv"))
    recovered_world.to_csv(os.path.join(DATA_DIR,"Recovered World.csv"))
    confirmed_usa.to_csv(os.path.join(DATA_DIR,"Confirmed USA.csv"))
    dea_usa.to_csv(os.path.join(DATA_DIR,"Death USA.csv"))
 
def check_date_file():
    date_today = (dt.datetime.now()-dt.timedelta(days=1)).strftime("%#m/%d/%y")
 
    for file in os.listdir(DATA_DIR):
        not_updated_files = []
        data = pd.read_csv(os.path.join(DATA_DIR,file))
        last_col = data.columns[-1]
 
        if last_col != date_today:
            not_updated_files.append(file)
    return not_updated_files
 
def delete_data_files():
    for file in os.listdir(DATA_DIR):
        os.remove(os.path.join(DATA_DIR,file))
 
def check_and_gather_file():
    if len(os.listdir(DATA_DIR))==0:
        download_files()
    elif len(check_date_file())>=1:
        delete_data_files()
        download_files()

def create_moving_average(df,windows,metric,grouping=False,grouping_col=""):
    if grouping:
        for window in windows:
            df["MA"+window+"_"+metric] = df.groupby(grouping_col)[metric] \
                                .transform(lambda x: x.rolling(int(window), 1).mean())\
                                .fillna(0)
        return df
    else:
        for window in windows:
            df["MA"+window+"_"+metric] = df[metric] \
                                .rolling(window=int(window)) \
                                .mean() \
                                .fillna(0)
        return df


def generate_data():
    confirmed_usa   =  pd.read_csv(os.path.join(DATA_DIR,"Confirmed USA.csv"))
    confirmed_world =  pd.read_csv(os.path.join(DATA_DIR,"Confirmed World Cases.csv"))
    death_usa       =  pd.read_csv(os.path.join(DATA_DIR,"Death USA.csv"))
    death_world     =  pd.read_csv(os.path.join(DATA_DIR,"Death World Cases.csv"))
    recovered_world =  pd.read_csv(os.path.join(DATA_DIR,"Recovered World.csv"))
    
    
    confirmed_usa.rename(columns={'Unnamed: 0':"Province"},inplace=True)
    confirmed_world.rename(columns={'Unnamed: 0':"Province"},inplace=True)
    death_usa.rename(columns={'Unnamed: 0':"Province"},inplace=True)
    death_world.rename(columns={'Unnamed: 0':"Province"},inplace=True)
    recovered_world.rename(columns={'Unnamed: 0':"Province"},inplace=True)
 
    # Get Total confirmed - USA
    last_date_col = confirmed_usa.columns[-1]
    total_confirmed_usa = confirmed_world[["Country/Region",last_date_col]] \
                      .query("`Country/Region`=='US'") \
                      .values[0][1]  
    total_confirmed_usa = pd.DataFrame({"date":last_date_col,
                                    "confirmed": total_confirmed_usa, 
                                    "level":"world","countries": "usa"}, 
                                   index=[0])
   # Get Total Confirmed - World
    total_confirmed_world = confirmed_world[last_date_col].sum()    
    total_confirmed_world = pd.DataFrame({"date":last_date_col,
                                      "confirmed": total_confirmed_world, 
                                      "level":"world",
                                      "countries": "world"}, 
                                     index=[0])
    
    # Get Deaths - USA
    total_death_usa = death_world[["Country/Region",last_date_col]] \
                  .query("`Country/Region`=='US'") \
                  .values[0][1]
    total_death_usa =  pd.DataFrame({"date":last_date_col,
                                        "confirmed": total_death_usa, 
                                        "level":"world",
                                        "countries": "usa"}, 
                                        index=[0])

    total_death_world = death_world[last_date_col].sum()
    total_death_world = pd.DataFrame({"date":last_date_col,
                                        "deaths": total_death_world, 
                                        "level":"world",
                                        "countries": "world"}, 
                                        index=[0])

    total_recovered_world = recovered_world[last_date_col].sum()
    total_recovered_world = pd.DataFrame({"date":last_date_col,
                                        "recoveries": total_recovered_world, 
                                        "level":"world",
                                        "countries": "world"}, 
                                        index=[0])
    metrics = pd.concat([total_confirmed_usa,
                    total_confirmed_world,
                    total_death_world,
                    total_death_usa,
                    total_recovered_world],
                    ignore_index=False) \
                    .reset_index(drop=True) \
                    .fillna(0)
        
    
    # Run MA16, MA30, and MA90 - Confirmed Cases - World
    date_cols = [col for col in confirmed_usa.columns if col.find("/")>0]
    
    confirm_pool_usa=confirmed_usa[["Province_State"]+date_cols] \
                    .melt(id_vars="Province_State")\
                    .rename(columns={"variable":"Date","value":"confirmed"}) \
                    .groupby(by=["Province_State","Date"]) \
                    .sum() \
                    .reset_index() \
                    .assign(level="usa", countries="usa") \
                    .rename(columns={"Date":"date","Confirmed":"confirmed",
                            "Province_State":"region"})
    confirm_pool_usa["date"] = pd.to_datetime(confirm_pool_usa["date"])
    
    
    
    confirm_pool_world=confirmed_world[["Country/Region"]+date_cols] \
                    .melt(id_vars="Country/Region")\
                    .rename(columns={"variable":"Date","value":"Confirmed"}) \
                    .groupby(by=["Country/Region","Date"]) \
                    .sum() \
                    .reset_index() \
                    .assign(level="world", countries="all") \
                    .rename(columns={"Date":"date","Confirmed":"confirmed",
                            "Country/Region":"region"})
                    
    confirm_pool_world["date"] = pd.to_datetime(confirm_pool_world["date"])
    confirms = pd.concat([confirm_pool_world,confirm_pool_usa],ignore_index=True)
    
    confirms["confirms_change"]=confirms.groupby(by="region")["confirmed"] \
                .apply(lambda x: x-x.shift(1))
    
    confirms= create_moving_average(confirms,
                                        ["14","30","90"],
                                        "confirms_change",
                                        grouping=True,
                                        grouping_col="level")
    
    # Deaths
    usa_deaths = death_usa[["Province_State"]+date_cols] \
                    .melt(id_vars="Province_State")\
                    .rename(columns={"variable":"date","value":"deaths"}) \
                    .groupby(by=["Province_State","date"]) \
                    .sum() \
                    .reset_index() \
                    .assign(level="usa", countries="usa") \
                    .rename(columns={"Province_State":"region"})
    
    world_deaths = death_world[["Country/Region"]+date_cols] \
                    .melt(id_vars="Country/Region")\
                    .rename(columns={"variable":"date","value":"deaths"}) \
                    .groupby(by=["Country/Region","date"]) \
                    .sum() \
                    .reset_index() \
                    .assign(level="world", countries="world") \
                    .rename(columns={"Country/Region":"region"})
    
    usa_deaths["date"] = pd.to_datetime(usa_deaths["date"])
    world_deaths["date"] = pd.to_datetime(world_deaths["date"])
    deaths = pd.concat([usa_deaths,world_deaths],ignore_index=True)
    
    deaths["deaths_change"]=deaths.groupby(by="region")["deaths"] \
                .apply(lambda x: x-x.shift(1))
    
    
    deaths = create_moving_average(deaths,["14","30","90"],
                                        "deaths_change",
                                        grouping=True,
                                        grouping_col="level")
    
    # Recoveries
    recoveries = recovered_world[["Country/Region"]+date_cols] \
                    .melt(id_vars="Country/Region")\
                    .rename(columns={"variable":"date","value":"recoveries"}) \
                    .groupby(by=["Country/Region","date"]) \
                    .sum() \
                    .reset_index() \
                    .assign(level="world", countries="world") \
                    .rename(columns={"Country/Region":"region"})
    
    recoveries["recovery_change"]=recoveries.groupby(by="region")["recoveries"] \
                .apply(lambda x: x-x.shift(1))
    
    recoveries["date"] = pd.to_datetime(recoveries["date"])
    recoveries = create_moving_average(recoveries,["14","30","90"],
                                        "recovery_change",
                                        grouping=True,
                                        grouping_col="region")
    

    # Calculate Global Metrics
    global_confirm = confirm_pool_world[["date","confirmed"]] \
                    .groupby("date") \
                    .sum() \
                    .rename(columns={"confirmed":"value"}) \
                    .assign(metric="confirmed") \
                    .reset_index()

    global_confirm[["change"]] = global_confirm[["value"]] \
                    .apply(lambda x: x-x.shift(1)) \
                    .fillna(0)

    global_deaths =  world_deaths[["date","deaths"]] \
                    .groupby("date") \
                    .sum() \
                    .rename(columns={"deaths":"value"}) \
                    .assign(metric="deaths") \
                    .reset_index()
                    
    global_deaths[["change"]] = global_deaths[["value"]] \
                    .apply(lambda x: x-x.shift(1)) \
                    .fillna(0)

    global_recovery = recoveries[["date","recoveries"]] \
                    .groupby("date") \
                    .sum() \
                    .rename(columns={"recoveries":"value"}) \
                    .assign(metric="recovery") \
                    .reset_index()

    global_recovery[["change"]] = global_recovery[["value"]] \
                    .apply(lambda x: x-x.shift(1)) \
                    .fillna(0)

    global_confirm  = create_moving_average(global_confirm,
                                            ["14","30","90"],
                                            "change")

    global_deaths  = create_moving_average(global_deaths,
                                            ["14","30","90"],
                                            "change")

    global_recovery  = create_moving_average(global_recovery,
                                            ["14","30","90"],
                                            "change")

    globally = pd.concat([global_confirm,
                        global_deaths,
                        global_recovery], 
                        ignore_index=False)

    globally[["MA14_change","MA30_change","MA90_change"]] = \
            globally[["MA14_change","MA30_change","MA90_change"]] \
        .apply(lambda x: np.round(x,1),axis=1)
                    

    # Combine and round MA Columns
    all=pd.merge(confirms,deaths,left_on=["region","date"],
            right_on=["region","date"],how="left")
    
    all=all.merge(recoveries,left_on=["region","date"],
            right_on=["region","date"],how="left")

    drops = [col for col in all.columns if col.find("_y")>0]
    
    all.drop(columns=drops,inplace=True)
    
    MA_cols = [col for col in all.columns if col.find("MA")>=0]
    all[MA_cols]=all[MA_cols].apply(lambda x: np.round(x,1),axis=1)
    all.columns=[re.sub("_[A-Za-z]?$", "", col) for col in all.columns]

    # # Override into database
    with open(os.path.join(BASE_DIR,"config.json")) as f:
        credentials = json.load(f)
    engine = create_engine("postgresql://{0}:{1}@localhost:5432/covid".format(credentials["username"], credentials["password"]))
    con = engine.connect()
    all.to_sql("metrics",con,if_exists="replace")
    globally.to_sql("globals",con,if_exists="replace")
    con.close()
    engine.dispose()


def exp(x, a, b, c):
    return a * np.exp(-b * x) + c

def calc_predictions():
    # First calculate the predictions for the entire world

    with open(os.path.join(BASE_DIR,"config.json")) as f:
        credentials = json.load(f)
    con =  sql.connect(dbname="covid",user=credentials["username"],password=credentials["password"])
    cur = con.cursor()
    cur.execute(""" 
      select date,
            sum(case when metric='confirmed'then value end) as confirms, 
            sum(case when metric='deaths'then value end) as deaths,
            sum(case when metric='recovery'then value end) as recovered
        from globals 
        where extract(year from date)= extract(year from now())
        group by date
        order by date;
    """)

    res   = [row for row in cur.fetchall()]
    world = pd.DataFrame(res, columns=[field[0] for field in cur.description])

    cur.close()
    con.close()

    world["index"]=np.arange(1,world.shape[0]+1)
    end=dt.date(dt.date.today().year, 12,31)
    start = world["date"].min()
    start = dt.date(year=start.year, month = start.month, day = start.day)
    total_days=(end-start).days

    preds = {"confirms":0,"deaths":0,"recovered":0}

    for metric in ["confirms","deaths","recovered"]:
        popt, pcov    = curve_fit(exp, world["index"], world[metric],maxfev=10000)
        preds[metric] = round(exp(total_days, *popt),1)
    
    preds["region"]="world"
    pred_df = pd.DataFrame([preds])

    # Get predictions by country
    con =  sql.connect(dbname="covid",user=credentials["username"],password=credentials["password"])
    cur = con.cursor()
    cur.execute(""" 
      select 
            region,
            date,
            confirmed as confirms, 
            deaths,
            recoveries as recovered
        from metrics
        where extract(year from date)= extract(year from now())
        order by region,date;
    """)

    res     = [row for row in cur.fetchall()]
    regions = pd.DataFrame(res, columns=[field[0] for field in cur.description])

    cur.close()
    con.close()

    preds_list=[]
    for region in regions["region"].unique():
        preds={}
        sub_df = regions.loc[regions["region"]==region,:]
        sub_df["index"]=np.arange(1,sub_df.shape[0]+1)
        sub_df=sub_df.fillna(0)
        for metric in ["confirms","deaths","recovered"]:
            popt, pcov    = curve_fit(exp, sub_df["index"], sub_df[metric],maxfev=10000)
            preds[metric] = round(exp(total_days, *popt),1)
        
        preds["region"]=region
        preds_list.append(preds)
   
    pred_df=pred_df.append(preds_list,ignore_index=True)
    
    engine = create_engine("postgresql://{0}:{1}@localhost:5432/covid".format(credentials["username"], 
                            credentials["password"] ))
    con = engine.connect()
    pred_df.to_sql("prediction",con,if_exists="replace")
    con.close()
    engine.dispose()


if __name__=="__main__":
    check_and_gather_file()
    generate_data()
    calc_predictions()