import pandas as pd 
import numpy as np 
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
from flask import Flask
import os
import datetime as dt
import psycopg2 as sql 
from dash.dependencies import Input, Output
import dash_table
import json

server = Flask(__name__)
app = dash.Dash(__name__, server=server, url_base_pathname='/', meta_tags=[
        {"name": "viewport", "content": "width=device-width, initial-scale=1"}
    ])

# Project and data paths
BASE_DIR = os.path.dirname(os.path.abspath(__name__))
DATA_DIR = os.path.join(BASE_DIR,"data")

def connect_db():
    """ Establish a connection to db """
    with open(os.path.join(BASE_DIR,"config.json")) as f:
        credentials = json.load(f)
    return sql.connect(dbname="covid",user=credentials["username"],password=credentials["password"])

def close_db(cur,con):
    """ Close connection to db """
    cur.close()
    con.close()

def create_result(cur, sql=""):
    """ Run query if returning result set """
    try:
        cur.execute(sql)
        res = [row for row in cur.fetchall()]
        cols = [field[0] for field in cur.description]
        return pd.DataFrame(res, columns=cols)
    except:
        print("Cannot execute query")
        print(sql)

""" Gather preliminary metrics on the world data """
con = connect_db()
cur = con.cursor()
global_metrics = create_result(cur,"""SELECT metric, value 
                                      FROM globals where date IN 
                                      (SELECT max(date) FROM globals);""")
total_cases = global_metrics.loc[global_metrics["metric"]=="confirmed","value"].values[0]
deaths      = global_metrics.loc[global_metrics["metric"]=="deaths","value"].values[0]
recovery    = global_metrics.loc[global_metrics["metric"]=="recovery","value"].values[0]
global_metrics["metric"] = global_metrics["metric"].str.title()

last_update = pd.to_datetime(create_result(cur, "select max(date) as date from globals;").values[0][0])

all_countries = create_result(cur,"""
        (SELECT DISTINCT CASE WHEN region='US' then 'US' ELSE INITCAP(region) END as label, region 
        AS value FROM metrics 
        UNION ALL 
        SELECT INITCAP('world') AS label, 'World' as value) ORDER BY label;""")

country_metrics = create_result(cur,"select region, date, confirmed, deaths, recoveries from metrics;")

country_stats = pd.read_csv(os.path.join(DATA_DIR,"country_wise_latest.csv"), 
                            usecols=["Country/Region","Confirmed","Deaths","Recovered"])
country_codes =  pd.read_csv(os.path.join(DATA_DIR,"country_codes.csv"))
country_stats=country_stats.merge(country_codes,left_on=["Country/Region"], 
                right_on=["Covid_Country"]) \
                .drop(columns=["Covid_Country"]) 
                
# Map 
fig = px.choropleth(country_stats, locations="iso_code",
                    color="Confirmed", # lifeExp is a column of gapminder
                    hover_name="Country/Region", # column to add to hover information
                    color_continuous_scale=px.colors.sequential.Reds,
                    title="WorldWide Distribution")
fig.update_layout(title_x=0.5)
close_db(cur,con)

# Dashboard HTML Layout
app.layout = html.Div(children=[
    html.Div(id="header", children=[
        html.H1(children="Covid-19 Dashboard"),
    ]),
    html.Div(id="country-selection", children=[
        html.P("Last Update: {0}".format(last_update.strftime("%m/%d/%Y")), id="update-date"),
        dcc.Dropdown(
        id='country-dropdown',
        options=all_countries.to_dict('records'),
        value='World'
            ),
    ]
        ),
    html.Div(id="overall-metrics", children=[
        html.P(id="total-cases"),
        html.P(id="total-deaths"),
        html.P(id="total-recovery")
    ],
        className="overall"),

    dcc.Graph(id='overall-graph'),

    html.Div(id="trends",
             children=[
                   dcc.Graph(id='case-trend'),
                   dcc.Graph(id='case-death'),
                   dcc.Graph(id='case-recovery'),
             ]
    ),
    html.Div(id="map",
            children=[
                dcc.Graph(id="distribution-map",
                figure=fig
                ),
            ]
    ),
    html.Div(
        id="variances",
        children=[
            dcc.Graph(id="confirms-variances"),
            dcc.Graph(id="death-variances"),
            dcc.Graph(id="recovery-variances"),
        ]
    ),
    html.Div(
        id="predictions",
        children = [
            html.Table([
                html.Tr([html.Th("Category"),html.Th("Prediction") ]),
            ]),
        ]),
    html.Footer(
        id="footer",
        children=[
            html.P(["In Affiliation with ", html.Span(html.A("raj302.com", href="https://www.raj302.com",id="affiliated" ))]),
        ]
    ),
    ])


@app.callback(
     Output(component_id='update-date', component_property='children'),
    [Input(component_id='country-dropdown', component_property='value')]
)
def update_date(input_value):
    con = connect_db()
    cur = con.cursor()
    last_update = pd.to_datetime(create_result(cur, "select max(date) as date from globals;").values[0][0])
    close_db(cur,con)
    return "Last Update: {0}".format(last_update.strftime("%m/%d/%Y"))

@app.callback(
    [Output(component_id='total-cases', component_property='children'),
    Output(component_id='total-recovery', component_property='children'),
     Output(component_id='total-deaths', component_property='children')],
    [Input(component_id='country-dropdown', component_property='value')]
)
def update_overall_metrics(input_value):
    """ Get the total cases, recoveries, and deaths for the region selected """
    if input_value=="World":
        return [f"Total Cases: {total_cases:,}",
                f"Total Recovered: {recovery:,}",
                f"Total Deaths: {deaths:,}"]
    else:      
        metrics=country_metrics.loc[country_metrics["region"]==input_value,
                        ["confirmed","deaths","recoveries"]] \
                        .tail(n=1) \
                        .fillna(0) \
                        .values[0]
        cases     = metrics[0]
        dead      = metrics[1]
        recover   = metrics[2]

        return [f"Total Cases: {cases:,}",
                f"Total Recovered: {recover:,}",
                f"Total Deaths: {dead:,}"
               ]
    
@app.callback(
    Output(component_id='overall-graph', component_property='figure'),
    [Input(component_id='country-dropdown', component_property='value')]
)
def update_overall_plot(input_value):
    """ Plot a bar chart showing total figures by total cases, recovered, and deaths"""
    if input_value=="World": 
        fig = px.bar(global_metrics, x="metric", y="value",title="<b>Overall COVID-19 Metrics</b>", 
            labels={"metric":"Metric","value":"Value"})
        fig.update_layout(title_x=0.5)
        fig.update_traces(marker_color=["orange","red","blue"])
        return fig
    else:
        fig_data = country_metrics.loc[country_metrics["region"]==input_value,
                        ["confirmed","deaths","recoveries"]] \
                        .tail(n=1) \
                        .fillna(0) \
                        .reset_index(drop=True) \
                        .T \
                        .reset_index() \
                        .rename(columns={"index":"metric",0:"value"})
        fig_data["metric"] = fig_data["metric"].str.title()

        fig = px.bar(fig_data, x="metric", y="value",title="<b>Overall COVID-19 Metrics</b>", 
                labels={"metric":"Metric","value":"Value"})
        fig.update_layout(title_x=0.5)
        fig.update_traces(marker_color=["orange","red","blue"])
        return fig
                
    return fig


@app.callback(
    [Output(component_id='case-trend', component_property='figure'),
     Output(component_id='case-death', component_property='figure'),
     Output(component_id='case-recovery', component_property='figure'),
    ],
    [Input(component_id='country-dropdown', component_property='value')]
)
def update_trends_plot(input_value):
    """ Generate trend plots by date for total cases, recoveries, and deaths """
    if input_value=="World": 
        con = connect_db()
        cur = con.cursor()
        world_trends = create_result(cur, """ 
                SELECT date,
                    SUM(case when metric='confirmed'then value end) AS confirm, 
                    SUM(case when metric='deaths'then value end) AS deaths,
                    SUM(case when metric='recovery'then value end) AS recovered
                FROM globals GROUP BY date ORDER BY date;
        """)
        close_db(cur,con)
        return generate_trend_plots(world_trends)
    else:
        con = connect_db()
        cur = con.cursor()
        region_trends = create_result(cur, """ 
                 SELECT region, date, sum(confirmed) AS confirm, 
                 sum(deaths) AS deaths, 
                 sum(recoveries) AS recovered 
                 FROM metrics WHERE region='{0}' GROUP BY region, date;
        """.format(input_value)) 
        close_db(cur,con)
        return generate_trend_plots(region_trends)
        

def generate_trend_plots(df):
    """ Generate actual line charts for each trend based on the dataframe passed in """
    case_fig = px.line(df, x="date", y="confirm",title="<b>Confirmations Trend</b>",
                labels={"date":"Date","confirm":"Confirmations"})
    case_fig.update_layout(title_x=0.5)
    case_fig.update_traces(line_color="orange")

    death_fig = px.line(df, x="date", y="deaths",title="<b>Death Trend</b>",
        labels={"date":"Date","deaths":"Deaths"})
    death_fig.update_layout(title_x=0.5)
    death_fig.update_traces(line_color="red")

    recovery_fig = px.line(df, x="date", y="recovered",title="<b>Recovery Trend</b>",
        labels={"date":"Date","recovered":"Recovered"})
    recovery_fig.update_layout(title_x=0.5)
    recovery_fig.update_traces(line_color="blue")

    return [case_fig,death_fig,recovery_fig]


# Add in variance plots with MAs
@app.callback(
    [Output(component_id='confirms-variances', component_property='figure'),
     Output(component_id='death-variances', component_property='figure'),
     Output(component_id='recovery-variances', component_property='figure')],
    [Input(component_id='country-dropdown', component_property='value')]
)
def update_variance_plots(input_value):
    """ Show the changes in cases for the 3 metrics IE confirms, deaths, and recoveries """
    if input_value=="World":
        con = connect_db()
        cur = con.cursor()
        world_variances = create_result(cur, """ 
               SELECT date,
                    SUM(case when metric='confirmed' then change end) AS confirm_change, 
                    SUM(case when metric='deaths' then change end) AS deaths_change,
                    SUM(case when metric='recovery' then change end) AS recovered_change,

                    SUM(case when metric='confirmed' then "MA14_change" end) AS MA14_confirm_change , 
                    SUM(case when metric='confirmed' then "MA30_change" end) AS MA30_confirm_change,
                    SUM(case when metric='confirmed' then "MA90_change" end) AS MA90_confirm_change,

                    SUM(case when metric='deaths'then "MA14_change" end) AS MA14_death_change , 
                    SUM(case when metric='death'then "MA30_change" end) AS MA30_death_change,
                    SUM(case when metric='death'then "MA90_change" end) AS MA90_death_change,

                    SUM(case when metric='recovery'then "MA14_change" end) AS MA14_recovery_change , 
                    SUM(case when metric='recovery'then "MA30_change" end) AS MA30_recovery_change,
                    SUM(case when metric='recovery'then "MA90_change" end) AS MA90_recovery_change
                    FROM globals GROUP BY date ORDER BY date;
        """)
        close_db(cur,con)
        return generate_variance_plots(world_variances)

    else:
        con = connect_db()
        cur = con.cursor()
        region_variances = create_result(cur, """    
            SELECT date,
                confirms_change AS confirm_change,
                deaths_change,
                recovery_change AS recovered_change,
                "MA14_confirms_change" AS ma14_confirm_change,
                "MA30_confirms_change" AS ma30_confirm_change,
                "MA90_confirms_change" AS ma90_confirm_change,
                "MA14_deaths_change" AS ma14_death_change,
                "MA30_deaths_change" AS ma30_death_change,
                "MA90_deaths_change" AS ma90_death_change,
                "MA14_recovery_change" AS ma14_recovery_change,
                "MA30_recovery_change" AS ma30_recovery_change,
                "MA90_recovery_change" AS ma90_recovery_change
            FROM metrics WHERE region = '{0}' order by date;""".format(input_value))
        close_db(cur,con)
        return generate_variance_plots(region_variances)


def generate_variance_plots(df):
    """ Generate change in cases plots """
    confirm_fig = px.line(df, x="date", y="confirm_change",title="<b>Confirmations Variance Trend</b>", 
                labels={"date":"Date","confirm_change":"Daily Change in Total Cases"})
    
    confirm_fig.add_scatter(x=df["date"], y= df["ma14_confirm_change"], mode="lines",name="MA14")
    confirm_fig.add_scatter(x=df["date"], y= df["ma30_confirm_change"], mode="lines",name="MA30")
    confirm_fig.add_scatter(x=df["date"], y= df["ma90_confirm_change"], mode="lines",name="MA90")

    confirm_fig.update_layout(title_x=0.5)

    death_fig = px.line(df, x="date", y="deaths_change",title="<b>Deaths Variance Trend</b>", 
            labels={"date":"Date","deaths_change":"Daily Change in Deaths"})
    
    death_fig.add_scatter(x=df["date"], y= df["ma14_death_change"], mode="lines",name="MA14")
    death_fig.add_scatter(x=df["date"], y= df["ma30_death_change"], mode="lines",name="MA30")
    death_fig.add_scatter(x=df["date"], y= df["ma90_death_change"], mode="lines",name="MA90")

    death_fig.update_layout(title_x=0.5)

    rec_fig = px.line(df, x="date", y="recovered_change",title="<b>Recovery Trend</b>", 
            labels={"date":"Date","recovered_change":"Daily Recovery Change"})

    rec_fig.add_scatter(x=df["date"], y= df["ma14_recovery_change"], mode="lines",name="MA14")
    rec_fig.add_scatter(x=df["date"], y= df["ma30_recovery_change"], mode="lines",name="MA30")
    rec_fig.add_scatter(x=df["date"], y= df["ma90_recovery_change"], mode="lines",name="MA90")
    rec_fig.update_layout(title_x=0.5)

    return [confirm_fig,death_fig,rec_fig]


@app.callback(
    Output(component_id='predictions', component_property='children'),
    [Input(component_id='country-dropdown', component_property='value')]
)
def update_pred_table(input_value):
    """ Generate predictions for the region selected"""
    con = connect_db()
    cur = con.cursor()
    input_value = input_value.lower() if input_value=="World" else input_value
    pred_df    = create_result(cur,"SELECT confirms, deaths, recovered FROM prediction WHERE region ='{0}';".format(input_value))
    confirms   = round(pred_df["confirms"].values[0],0)
    deaths     = round(pred_df["deaths"].values[0],0)
    recovered  = round(pred_df["recovered"].values[0],0)

    close_db(cur,con)

    return  html.Table([
                html.Tr([html.Th("Category"),html.Th("YR End Prediction") ]),
                html.Tr([html.Th("Confirmed"),html.Th(f"{confirms:,}") ]),
                html.Tr([html.Th("Deaths"),html.Th(f"{deaths:,}")]),
                html.Tr([html.Th("Recoveries"),html.Th(f"{recovered:,}") ]),
            ])

        
if __name__=="__main__":
    server.run(debug=True)