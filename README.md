### Intro
---
This project is a dashboard built using **Plotly** and **Dash** that shows various trends such as total cases, deaths, and recovery. This 
dashboard allows the user to select by region or country as well and the dashboard will update accordingly. Various statistical methods such 
as moving averages are used to trend out total cases, deaths, and recoveries within a particular region or globally. Also an exponential model 
is used to predict the total cases etc by year end and updated as well on a daily basis where the data is processed and stored in a postgresql database. 
The project is live and can be visited at [here](https://covid19.raj302.com).  The various technlogies used in this project: 

1. **Flask**
2. **Python 3.7**
3. **uWSGI**
4. **nginx**
5. **Plotly**
6. **Ubuntu 18.04**
7. **Postgresql**

### Setup 
--- 
Note that dash apps use flask under the hood. Before you start ensure that you setup a **config.json** 
file with the following structure below prior to setup: 

`{"username":<username>,
   "password": <password>	 
}`
From here you can setup your virtual environment and download the required packages using pip and the **requirements.txt**
file. Setup nginx to handle requests and setup the **covid.service** file to run as a service in the background. 
**uWSGI.py** is the entry point for the dashboard.  You can also setup **metrics.py** on the cron system to run on 
a daily basis and update the data in your database. Take a look at the link below that goes over in more detail
how to setup a flask application on a linux server. 

https://www.digitalocean.com/community/tutorials/how-to-serve-django-applications-with-uwsgi-and-nginx-on-ubuntu-16-04


